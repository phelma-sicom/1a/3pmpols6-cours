<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 💻 TD : *Travailler avec des images en informatique*

L'objectif de ce TD est de travailler avec des images pour **mettre en pratique** les notions vues dans le cours :
- Analyser la **structure matricielle** des images
- Comprendre les **histogrammes**
- Comprendre les **filtres de convolution**

```{note}
Différentes possibilités de travail dans l'**écosystème Jupyter** pour les TD :
- Soit en utilisant les **ressources de votre ordinateur** (Jupyter Lite)

<img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/>

- Soit en utilisant les **ressources d'un serveur distant** de [GRICAD](https://gricad.univ-grenoble-alpes.fr/) avec vos identifiants étudiants AGALAN (Jupyter Hub)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/>

- Soit en utilisant les **ressources d'un serveur distant** (Jupyter Hub - My Binder)

<img src="https://mybinder.org/badge_logo.svg" alt="Jupyter Hub - My Binder" height="25px"/>

- Soit en **téléchargeant le TD** et l'utilisant dans un environnement **Jupyter local** :

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/>

- Soit en utilisant **Colaboratory** (nécessite un compte Google pour exécuter les cellules mais pas pour les visualiser) :

<img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>

Pour mieux comprendre le fonctionnement de l'écosystème Jupyter, vous pouvez regarder les <a href="./Ressource_2.html">ressources complémentaires 🪐</a>
```
___

## TD 1 - Partie 1

Cette **première partie du TD 1** traite de la **structure matricielle des images** et des **histogrammes** pour les analyser et les modifier.

- Exécution sur votre ordinateur (nécessite un environnement Python de data science) :

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_1.ipynb?inline=false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a> <a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_1.ipynb?inline=false">Téléchargement du fichier du notebook du TD</a>

- Exécution sur votre ordinateur (pas d'installation nécessaire) :

<a href="https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/1a/3pmpols6-td/retro/notebooks/?path=TD_1_1.ipynb" target="_blank"><img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/></a>

- Exécution sur un serveur distant [GRICAD](https://gricad.univ-grenoble-alpes.fr/) avec Jupyter Hub (nécessite vos identifiants AGALAN) :

<a href="https://gricad-jupyter.univ-grenoble-alpes.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F1a%2F3pmpols6-td&urlpath=lab%2Ftree%2F3pmpols6-td%2Fcontent%2FTD_1_1.ipynb&branch=main"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a>

- Exécution sur un serveur distant MyBinder (pas d'installation nécessaire) :

<a href="https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F1a%2F3pmpols6-td/main?labpath=content%2FTD_1_1.ipynb" target="_blank"><img src="https://mybinder.org/badge_logo.svg" alt="Jupyter Hub - My Binder" height="25px"/></a>

- Exécution sur un serveur distant Google - Colaboratory (compte Google nécessaire pour exécuter les cellules) :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/TD_1_1.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### 🔎 Prévisualisation du TD 1 - Partie 1

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_1.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 

___

## TD 1 - Partie 2

Cette **deuxième partie du TD 1** traite des **noyaux de convolutions** et leur application sur des images.

- Exécution sur votre ordinateur (nécessite un environnement Python de data science) :

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_2.ipynb?inline=false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a> <a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_2.ipynb?inline=false">Téléchargement du fichier du notebook du TD</a>

- Exécution sur votre ordinateur (pas d'installation nécessaire) :

<a href="https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/1a/3pmpols6-td/retro/notebooks/?path=TD_1_2.ipynb" target="_blank"><img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/></a>

- Exécution sur un serveur distant [GRICAD](https://gricad.univ-grenoble-alpes.fr/) avec Jupyter Hub (nécessite vos identifiants AGALAN) :

<a href="https://gricad-jupyter.univ-grenoble-alpes.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F1a%2F3pmpols6-td&urlpath=lab%2Ftree%2F3pmpols6-td%2Fcontent%2FTD_1_2.ipynb&branch=main"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a>

- Exécution sur un serveur distant MyBinder (pas d'installation nécessaire) :

<a href="https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F1a%2F3pmpols6-td/main?labpath=content%2FTD_1_2.ipynb" target="_blank"><img src="https://mybinder.org/badge_logo.svg" alt="Jupyter Hub - My Binder" height="25px"/></a>

- Exécution sur un serveur distant Google - Colaboratory (compte Google nécessaire pour exécuter les cellules) :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/TD_1_2.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

### 🔎 Prévisualisation du TD 1 - Partie 2

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/TD_1_2.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 