# Module de préorientation - SICOM : Traitement du Signal et Multimedia - 3PMPOLS6

```{sidebar} 🎓 Cursus à Phelma
<a href="https://phelma.grenoble-inp.fr/medias/photo/schema-cursus_1652799196414-jpg?ID_FICHE=686041" target="_blank"><img src="https://phelma.grenoble-inp.fr/medias/photo/schema-cursus_1652799196414-jpg?ID_FICHE=686041" alt="Cours Phelma" /></a> 
```

Ce cours et ces TD font partie du module de préorientation <a href="https://phelma.grenoble-inp.fr/fr/formation/pr-eacute-orientation-sicom-traitement-du-signal-et-multimedia-3pmpols6" target="_blank">Traitement du Signal et Multimedia - 3PMPOLS6</a> pour la filière <a href="https://phelma.grenoble-inp.fr/fr/formation/ingenieur-de-grenoble-inp-phelma-filiere-signal-image-communication-multimedia-sicom#page-presentation" target="_blank">SICOM</a>. Plus précisément, ils traitent de la partie **Traitement d'image** de ce module.
___

L'**image** a pris une place de **plus en plus importante** dans nos vies ces dernières années. Très souvent ces nouveaux usages s'appuient sur de l'**intelligence artificielle**. On peut citer par exemple :

- La **détection de plantes** grâce à une photo (algorithme de deep learning de classification d'images)
- Le déverrouillage des smartphones par de la **reconnaissance faciale**
- La surveillance des foules par des **caméras intelligentes** lors de grands évènements comme les jeux olympiques (modèles de deep learning embarqués)
- Les **deep fakes** (modèles de deep learning génératifs)

Ces derniers exemples posent des questions **éthiques** sur l'usage de ces technologies. Un autre grand enjeu de ces modèles est leur **impact environnemental**. 

___

L'**histoire de l'intelligence artificielle** appliquée à l'**image** s'est accélérée ces dernières années. Voici les **dates clés** (le découpage par décennie est arbitraire) :

- La décennie **1940-1950**

<a href="https://fr.wikipedia.org/wiki/Warren_McCulloch" target="_blank"><img src="https://www.researchgate.net/profile/Ksenia-Fedorova/publication/317038989/figure/fig1/AS:496498733715456@1495385930447/Firings-in-the-memory-neuron-Source-Warren-McCulloch-Walter-Pitts-A-Logical.png" alt="Neurone" height="200px"/></a>

*Source de l'image : researchgate*

C'est dans cette décennie qu'on peut dater le **début de l'intelligence artificielle** même si le terme apparaît pour la première fois en 1956 à la <a href="https://fr.wikipedia.org/wiki/Conférence_de_Dartmouth" target="_blank">conférence de Dartmouth</a>. Warren McCulloch et Walter Pitts propose le **premier modèle mathématique** d'un **neurone biologique** en 1943.

📖 **Papier de recherche marquant** de la décennie **1940-1950** : {cite}`biological_neuron`

___

- La décennie **1950-1960**

<a href="https://en.wikipedia.org/wiki/Perceptron" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/330-PSA-80-60_%28USN_710739%29_%2820897323365%29.jpg/440px-330-PSA-80-60_%28USN_710739%29_%2820897323365%29.jpg" alt="Perceptron" height="200px"/></a>

*Source de l'image : wikipedia*

Franck Rosenblatt crée le **perceptron**. Il s'agit d'un modèle de **neurone artificiel simple** utilisé pour la classification binaire et basé sur une **combinaison linéaire des entrées pondérées** et une **fonction d'activation seuil**.

📖 **Papier de recherche marquant** de la décennie **1950-1960** : {cite}`perceptron`

___
- La décennie **1960-1970**

<a href="https://fr.wikipedia.org/wiki/Perceptron_multicouche" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/a/a9/Perceptron_4layers.png" alt="MLP" height="200px"/></a>

*Source de l'image : wikipedia*

La **descente de gradient stochastique** commence à être utilisée par des chercheurs pour **entrainer** des réseaux de neurones complétement connectés.

📖 **Papier de recherche marquant** de la décennie **1960-1970** : {cite}`perceptron_2`

___
- La décennie **1970-1980**

<a href="https://fr.wikipedia.org/wiki/Histoire_de_l%27intelligence_artificielle#La_premi%C3%A8re_hibernation_de_l'intelligence_artificielle_(1974%E2%88%921980)" target="_blank"><img src="https://cdn.pixabay.com/photo/2016/12/07/23/41/snow-1890653_1280.jpg" alt="Hiver" height="200px"/></a>

*Source de l'image : pixabay*

Le premier **hiver de l'intelligence artificielle** débute. Il est provoqué par des **attentes exagérées**, des **limites technologiques (puissances de calculs et volume de données)** et des **critiques** de certains chercheurs comme Marvin Minsky envers les **modèles connexionnistes**. Les financements et l'intérêt pour l'IA ont diminué à cette époque.

📖 **Papier de recherche marquant** de la décennie **1970-1980** : {cite}`epistemological_ia`

___
- La décennie **1980-1990**

<a href="https://fr.wikipedia.org/wiki/Machine_Lisp" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/LISP_machine.jpg/1024px-LISP_machine.jpg" alt="LISP" height="200px"/></a>

*Source de l'image : wikipedia*

Les **systèmes experts** qui utilisent des règles de connaissances pour **résoudre des problèmes spécifiques** sont devenus populaires et ont trouvé des applications dans **divers domaines**, de la médecine à la finance. Mais à la **fin des années 80**, le **deuxième hiver de l'intelligence artificielle** débute.

📖 **Papier de recherche marquant** de la décennie **1980-1990** : {cite}`neocognitron`

___
- La décennie **1990-2000**

<a href="http://yann.lecun.com/exdb/mnist/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/2/27/MnistExamples.png" alt="MNIST" height="200px"/></a>

*Source de l'image : wikipedia*

La vision par ordinateur commence à trouver des **applications importantes**. La détection des **écritures manuscrites** sur les chèques peut être effectuée grâce à des **réseaux de convolution** (Convolutional Neural Networks - CNN).

📖 **Papier de recherche marquant** de la décennie **1990-2000** : {cite}`gradient_recognition`

___
- La décennie **2000-2010**

<a href="https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html" target="_blank"><img src="https://docs.opencv.org/3.4/haar.png" alt="ImageNet" height="200px"/></a>

*Source de l'image : opencv*

Les **caractéristiques pseudo-Haar** qui ne s'appuient pas sur des réseaux de convolution donnent des résultats intéressants pour la détection des visages. Néanmoins, les applications des **réseaux de convolution** (Convolutional Neural Networks - CNN) commencent à se multiplier dans différents domaines comme la **reconnaissance d'objets**, à la **détection de visages** ou à la **conduite autonome**.

📖 **Papier de recherche marquant** de la décennie **2000-2010** : {cite}`cascade`

___
- La décennie **2010-2020**

<a href="https://image-net.org/index.php" target="_blank"><img src="https://image-net.org/static_files/index_files/logo.jpg" alt="ImageNet" height="50px"/></a>

*Source de l'image : imagenet*

La vision par ordinateur est à un **tournant** : l'apprentissage profond par des **réseaux de convolution** (Convolutional Neural Networks - CNN) permet d'apprendre les caractéristiques à partir des images (c'est ce que vous allez voir dans ce cours). Grâce à l'augmentation des **puissances de calculs** et des **volumes des images disponibles** ainsi que l'utilisation de **composants électroniques spécialisés** dans le calcul matriciel (<a href="https://fr.wikipedia.org/wiki/Processeur_graphique" target="_blank">GPU</a>, <a href="https://fr.wikipedia.org/wiki/Tensor_Processing_Unit" target="_blank">TPU</a>, <a href="https://fr.wikipedia.org/wiki/Application-specific_integrated_circuit" target="_blank">ASIC</a>...), les performances des algorithmes basés sur des **caractéristiques construites manuellement** sont dépassées par les **réseaux de neurones de convolutions (CNN)**.

📖 **Papier de recherche marquant** de la décennie **2010-2020** : {cite}`imagenet`

___
- La décennie **2020-aujourd'hui**

<a href="https://github.com/Stability-AI/generative-models" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/d/d3/Astronaut_Riding_a_Horse_%28SDXL%29.jpg" alt="Stable diffusion" height="150px"/></a>

*Source de l'image : wikipedia*

L'intelligence artificielle appliquée à l'image continue d'évoluer rapidement avec le développement des **algorithmes de génération d'images** (auto-encodeur variationnel (VAE), réseaux antagonistes génératifs (GAN), modèle de diffusion). Ces développements créent des préoccupations en particulier en matière d'**éthique**, d'**environnement**, de **désinformation** et de **vie privée**.

📖 **Papier de recherche marquant** de la décennie **2020-aujourd'hui** : {cite}`gan`

___
```{card} 🇪🇺 **Législation**

<a href="https://digital-strategy.ec.europa.eu/fr/policies/regulatory-framework-ai" target="_blank"><img src="https://ec.europa.eu/information_society/newsroom/image/document/2021-17/pyramid_7F5843E5-9386-8052-931F5C4E98C6E5F2_75757.jpg" alt="Législation" /></a>

*Source de l'image : commission européenne*

Etant donné l'**impact très important** de ces technologies d'**intelligence artificielle**, l'union européenne travaille à mettre en place des **lois pour réglementer l'intelligence artificielle** (<a href="https://www.europarl.europa.eu/news/fr/headlines/society/20230601STO93804/loi-sur-l-ia-de-l-ue-premiere-reglementation-de-l-intelligence-artificielle" target="_blank">AI act</a>) sur le modèle de ce qui a été fait pour les **données personnelles** avec le <a href="https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on" target="_blank">RGPD</a>. L'IA Act vise à établir un **cadre juridique** pour l'utilisation de l'intelligence artificielle, mettant l'accent sur la **protection des droits fondamentaux des citoyens** et la **promotion** de l'adoption de technologies d'**IA éthiques et responsables**. Le règlement classe l'IA en **quatre catégories principales** : IA à **risque inacceptables**, IA à **risque élevé** et IA à **risque limité** et IA à **risque minimal ou nul**. Les réglementations varient en fonction de la catégorie.
```