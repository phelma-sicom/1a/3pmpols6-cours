# 🐍 Bibliothèques Python utiles pour travailler avec des images

```{note}
Toutes les bibliothèques Python présentées sont **open-source**. Les **liens vers la documentation** sont accessibles en cliquant sur les logos des bibliothèques. 
```

## Bibliothèques généralistes

<a href="https://numpy.org" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/NumPy_logo_2020.svg/1200px-NumPy_logo_2020.svg.png" alt="Numpy" height="100px"/></a> 

La bibliothèque <a href="https://numpy.org" target="_blank">Numpy</a> est fondamentale pour le **calcul numérique** en Python. Elle offre un support efficace pour la manipulation de **tableaux multidimensionnels** (scalaires, vecteurs, matrices, tenseurs) et des **opérations mathématiques** sur ces tableaux. Elle est largement utilisée dans le domaine de la **science des données** (data science), de l'**apprentissage automatique** et de l'**analyse numérique**.

*Dans les activités de ce module, elle est utilisée pour appliquer des **transformations** sur des images.*

___
<a href="https://docs.scipy.org/doc/scipy/" target="_blank"><img src="https://docs.scipy.org/doc/scipy/_static/logo.svg" alt="Scipy" height="100px"/></a> 

La bibliothèque <a href="https://docs.scipy.org/doc/scipy/" target="_blank">Scipy</a>  fournit des **outils** et des **fonctions** pour la résolution de **problèmes scientifiques** et d'**ingénierie**, notamment l'optimisation, l'algèbre linéaire, la statistique, le traitement du signal... Elle s'appuie sur la bibliothèque **NumPy** en fournissant des fonctionnalités avancées pour l'**analyse de données** et la **modélisation numérique**.

*Dans les activités de ce module, elle est utilisée pour appliquer des **filtres de convolutions** sur des images.*
___
<a href="https://matplotlib.org" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/fr/thumb/3/37/Logo_Matplotlib.svg/langfr-2560px-Logo_Matplotlib.svg.png" alt="Matplotlib" height="100px"/></a> 

La bibliothèque <a href="https://matplotlib.org" target="_blank">Matplotlib</a> permet de créer des **graphiques de haute qualité**, des tracés **2D et 3D**, des **diagrammes**, des **histogrammes** et d'autres représentations graphiques de données de manière personnalisable. Elle est largement utilisée dans le domaine de la **science des données** (data science) et de la **recherche scientifique**.

*Dans les activités de ce module, elle est utilisée pour **afficher les images** et les **histogrammes d'intensités lumineuses**.*
___
<a href="https://seaborn.pydata.org" target="_blank"><img src="https://seaborn.pydata.org/_static/logo-wide-lightbg.svg" alt="Seaborn" height="100px"/></a> 

La bibliothèque <a href="https://seaborn.pydata.org" target="_blank">Seaborn</a> est construite au-dessus de la bibliothèque de visualisation de données **Matplotlib**. Elle aide à la création de graphiques en offrant des **fonctions simples**. Elle est particulièrement adaptée à la **visualisation de données statistiques complexes** et est couramment utilisée dans le domaine de l'**analyse de données** pour explorer et présenter des relations entre les variables.

*Dans les activités de ce module, elle est utilisée pour afficher les **histogrammes d'intensités lumineuses**.*
___
<a href="https://scikit-learn.org/stable/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Scikit_learn_logo_small.svg/1200px-Scikit_learn_logo_small.svg.png" alt="Scikit-learn" height="100px"/></a> 

La bibliothèque <a href="https://scikit-learn.org/stable/" target="_blank">Scikit-learn</a> permet de faire du **machine learning**. Elle offre une gamme d'outils efficaces pour l'**analyse de données** et la **modélisation statistique** : la classification, la régression, le clustering et la réduction de dimensionnalité.

*Dans les activités de ce module, elle est utilisée pour **calculer des métriques**.*

👨‍🏫 <a href="https://scikit-learn.org/stable/tutorial/index.html" target="_blank">Tutoriel pour utiliser Scikit-learn</a>

___
<a href="https://keras.io" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Keras_logo.svg/langfr-1280px-Keras_logo.svg.png" alt="Keras" height="100px"/></a> 

La bibliothèque <a href="https://keras.io" target="_blank">Keras</a> permet de créer des **réseaux de neurones artificiels**. Elle est réputée pour sa **simplicité** en fournissant une **interface haut niveau** pour la **construction**, l'**entrainement** et l'**évaluation** de modèles d'apprentissage profond (deep learning). Cette bibliothèque est très utilisée par les **développeurs** en apprentissage profond (deep learning). 

*Dans les activités de ce module, elle est utilisée pour **créer**, **entrainer** et **évaluer** des réseaux de neurones artificiels.*

👩‍🏫 <a href="https://keras.io/getting_started/intro_to_keras_for_engineers/" target="_blank">Tutoriel pour utiliser Keras</a>
___
<a href="https://www.tensorflow.org/?hl=fr" target="_blank"><img src="https://www.gstatic.com/devrel-devsite/prod/v7101fe1ae309bf6f8b73931812f2304140fb851e33f225a68507501988007b93/tensorflow/images/lockup.svg" alt="Tensorflow" height="100px"/></a>

La bibliothèque <a href="https://www.tensorflow.org/?hl=fr" target="_blank">TensorFlow</a> est  très populaire pour le **développement de modèles d'apprentissage profond (deep learning)**. Elle offre une **infrastructure flexible** pour la **création**, l'**entrainement** et le **déploiement de réseaux de neurones**. Elle contient aussi des **outils pour la manipulation de données** et des **fonctionnalités avancées d'optimisation**.

*Dans les activités de ce module, elle est utilisée pour **créer**, **entrainer** et **évaluer** des réseaux de neurones artificiels par l'intermédiaire de la bibliothèque Keras.*

👨‍🏫 <a href="https://www.tensorflow.org/tutorials?hl=fr" target="_blank">Tutoriel pour utiliser Tensorflow</a>
___
<a href="https://pytorch.org/get-started/locally/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/PyTorch_logo_black.svg/976px-PyTorch_logo_black.svg.png" alt="Pytorch" height="100px"/></a>

La bibliothèque <a href="https://pytorch.org/get-started/locally/" target="_blank">PyTorch</a> est très populaire pour le **développement de modèles d'apprentissage profond (deep learning)**. Elle se distingue par sa **flexibilité** et son **approche dynamique** de la construction de modèles. Elle offre une **interface conviviale** pour la **création**, l'**entrainement** et le **déploiement de réseaux de neurones**. Elle est largement utilisée pour des tâches telles que la **vision par ordinateur**, le **traitement du langage naturel (NLP)** et d'autres domaines de l'apprentissage profond.

*Dans les activités de ce module, elle n'est pas utilisée mais elle aurait pu être utilisée à la place de Tensorflow ou Keras. Elle sera utilisée durant les cours de troisième année.*

👩‍🏫 <a href="https://github.com/udacity/deep-learning-v2-pytorch" target="_blank">Tutoriel pour utiliser PyTorch</a>
___

## Bibliothèques spécialisées dans l'image

<a href="https://pillow.readthedocs.io/en/stable/" target="_blank"><img src="https://raw.githubusercontent.com/python-pillow/pillow-logo/main/pillow-logo-dark-text-1280x640.png" alt="Pillow" height="100px"/></a>

La bibliothèque <a href="https://pillow.readthedocs.io/en/stable/" target="_blank">Pillow</a> facilite la **manipulation d'images** notamment le **chargement**, la **modification** et la **sauvegarde d'images dans divers formats**. Elle offre des fonctionnalités puissantes pour le **traitement d'images** telles que le **redimensionnement**, la **rotation**, la **conversion de couleurs**, la **création de miniatures**... C'est un **outil essentiel** pour le **traitement d'images** dans de nombreuses applications Python.

*Dans les activités de ce module, elle est utilisée pour charger les images.*
___

<a href="https://docs.opencv.org/4.8.0/" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/5/53/OpenCV_Logo_with_text.png" alt="OpenCV" height="100px"/></a> 

La bibliothèque <a href="https://docs.opencv.org/4.8.0/" target="_blank">OpenCV</a> permet de faire de la **vision par ordinateur**. Elle offre un **large éventail de fonctionnalités** pour le **traitement d'images** et de **vidéos** : la **détection d'objets**, la **reconnaissance de formes**, la **segmentation d'images**, et la **manipulation d'images en temps réel**. Elle est largement utilisée dans des domaines comme la **robotique**, la **réalité augmentée** et la **surveillance vidéo** pour développer des **applications visuelles complexes**.

*Dans les activités de ce module, elle n'est pas utilisée.*
___
<a href="https://scikit-image.org" target="_blank"><img src="https://scikit-image.org/_static/img/logo.png" alt="scikit-image" height="100px"/></a> 

La bibliothèque <a href="https://scikit-image.org" target="_blank">Scikit-image</a> est spécialisée dans le **traitement d'images**. Elle offre un **ensemble d'outils et de fonctions** pour effectuer diverses opérations de **traitement d'images** : la **segmentation**, la **détection de contours**, la **mesure de formes**, la **restauration d'images**... Elle s'intègre bien avec d'autres bibliothèques scientifiques comme **NumPy** et **SciPy**.

*Dans les activités de ce module, elle n'est pas utilisée.*
___
<a href="https://keras.io/keras_cv/" target="_blank"><img src="https://opengraph.githubassets.com/2af5e1ea6368c7a0be1c289ee4625d41c280032a300f25c278c58eb7e0372d2b/keras-team/keras-cv" alt="KerasCV" height="100px"/></a>

La bibliothèque <a href="https://scikit-image.org" target="_blank">KerasCV</a> est une **extension** de la **bibliothèque Keras**. Elle propose une collection de modèles de **réseaux de neurones pré-entraînés** pour la **vision par ordinateur**, des outils pour **simplifier la création** et l'**évaluation** de modèles de deep learning pour des tâches de traitement d'images. Elle facilite la mise en oeuvre **rapide** de solutions de pointe en vision par ordinateur en tirant parti de **modèles pré-entraînés**.

*Dans les activités de ce module, elle n'est pas utilisée.*
___
<a href="https://mahotas.readthedocs.io/en/latest/index.html" target="_blank"><img src="https://opengraph.githubassets.com/97c04a93217e13ab130073ee89a74bff4059aa3354f12367b1ff798b3eefe21e/luispedro/mahotas" alt="Mahotas" height="100px"/></a>

La bibliothèque <a href="https://mahotas.readthedocs.io/en/latest/index.html" target="_blank">Mahotas</a> est spécialisée dans le **traitement d'images**. Elle offre des fonctionnalités pour effectuer des opérations de **traitement d'images avancées** : la **segmentation**, la **détection de caractéristiques**, la **transformation d'images** et l'**extraction d'objets**. Elle est souvent utilisée pour l'**analyse d'images** dans des **applications scientifiques** et **industrielles** en raison de sa rapidité et de sa polyvalence.

*Dans les activités de ce module, elle n'est pas utilisée.*

## Bibliothèques spécialisées dans l'augmentation d'images

Pour améliorer la **robustesse du modèle**, les images sont souvent modifiées par des **techniques d'augmentation** afin de générer des données d'entraînement **supplémentaires** à partir de l'ensemble existant.
___
<a href="https://imgaug.readthedocs.io/en/latest/" target="_blank"><img src="https://imgaug.readthedocs.io/en/latest/_images/heavy.jpg" alt="Imgaug" height="100px"/></a>

La bibliothèque <a href="https://imgaug.readthedocs.io/en/latest/" target="_blank">imgaug</a> est dédiée à l'**augmentation d'images**. Elle offre une grande variété de **transformations d'images** comme la rotation, le recadrage, le zoom, le bruit... Elle est hautement personnalisable et peut être intégrée facilement dans les **pipelines de prétraitement d'images**.

*Dans les activités de ce module, elle n'est pas utilisée.*
___
<a href="https://albumentations.ai/docs/" target="_blank"><img src="https://albumentations.ai/docs/images/logo.png" alt="Albumentations" height="100px"/></a>

La bibliothèque <a href="https://albumentations.ai/docs/" target="_blank">Albumentations</a> permet de faire de l'augmentation d'images hautement optimisée. Elle est spécialement conçue pour les tâches de **vision par ordinateur**. Elle offre une grande **variété de transformations d'images** et est réputée pour sa **rapidité**.

*Dans les activités de ce module, elle n'est pas utilisée.*
___
Les bibliothèques <a href="https://pillow.readthedocs.io/en/stable/" target="_blank">Pillow</a>, <a href="https://docs.opencv.org/4.8.0/" target="_blank">OpenCV</a> et <a href="https://www.tensorflow.org/tutorials/images/data_augmentation?hl=fr" target="_blank">Keras</a> permettent aussi de faire de l'**augmentation de données** pour le traitement d'images.
