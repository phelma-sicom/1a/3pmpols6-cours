<style>
      /* Pour la gestion des iframes */
 .iframe-wrapper {
  max-width: 60%;
  margin: auto; /* pour centrer le conteneur */
}

 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 🚀 Ressources pour aller plus loin

## Livres

```{note}
<a href="https://bibliotheques.univ-grenoble-alpes.fr" target="_blank"><img src="https://bibliotheques.univ-grenoble-alpes.fr/medias/photo/parvis-bujf-juin-2022_1654090924226-jpg" alt="BU Fourier" height="200px"/></a> 

**Tous les livres indiqués** dans cette partie sont **empruntables** avec votre carte d'étudiantes et d'étudiants dans les <a href="https://bibliotheques.univ-grenoble-alpes.fr" target="_blank">bibliothèques universitaires de Grenoble</a>.
```
<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/rgi2mt/alma990001629990306161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=0-201-18075-8/sc.jpg" alt="Livre Digital image processing" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/rgi2mt/alma990001629990306161" target="_blank">Digital image processing</a>, Gonzalez Rafael - Woods Richard, Pearson Education International, 2002 {cite}`alma990001629990306161`
___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990000966040306161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782746205840" alt="Livre Le traitement des images" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990000966040306161" target="_blank">Le traitement des images</a>, Maître Henri, Lavoisier, 2002 {cite}`alma990000966040306161`
___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990000988450306161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782225849237" alt="Livre Analyse d'images : filtrage et segmentation" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990000988450306161" target="_blank">Analyse d'images : filtrage et segmentation</a>, Cocquerez Jean-Pierre - Philipp-Foliguet Sylvie, Masson, 1995 {cite}`alma990000988450306161`
___

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990001052150306161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782746207417" alt="Livre Traitement et analyse des images numériques" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990001052150306161" target="_blank">Traitement et analyse des images numériques</a>, Bres Stéphane, Lavoisier, 2003 {cite}`alma990001052150306161`
___
<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006591685306161" target="_blank"><img src="https://bu.univ-grenoble-alpes.fr/FormAlmaElectre/electre2022b.php?ean=9782100790661" alt="Livre Deep learning" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma991006591685306161" target="_blank">Deep learning avec Keras et TensorFlow : mise en oeuvre et cas concrets</a>, Aurélien Géron, O'Reilly, 2020 {cite}`alma991006591685306161`
___
<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990005922390306161" target="_blank"><img src="https://proxy-euf.hosted.exlibrisgroup.com/exl_rewrite/syndetics.com/index.php?client=primo&isbn=978-1-492-03264-9/sc.jpg" alt="Livre Deep learning" height="100px"/></a> 

<a href="https://beluga.univ-grenoble-alpes.fr/permalink/33UGRENOBLE_INST/1vb34gl/alma990005922390306161" target="_blank">Hands-on machine learning with Scikit-Learn, Keras and TensorFlow : concepts, tools, and techniques to build intelligent systems</a>, Aurélien Géron, O'Reilly, 2019 {cite}`alma990005922390306161`
___

## Vidéos

<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/pxulkadbBnQ" title="3. 80 ans d&#39;histoire... et la victoire des neurones !" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/pxulkadbBnQ" target="_blank">80 ans d'histoire... et la victoire des neurones !</a>, CNRS - Formation FIDLE, 30min 58s, 2023
___
<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/BquRtCp9lvw" title="1. Intelligence Artificielle, Machine Learning, Deep-Learning... De quoi parle t-on au juste ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/BquRtCp9lvw" target="_blank">Intelligence Artificielle, Machine Learning, Deep-Learning... De quoi parle t-on au juste ?</a>, CNRS - Formation FIDLE, 15min 46s, 2023
___
<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/NmLK_WQBxB4?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI" title="MIT 6.S191: Convolutional Neural Networks" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/NmLK_WQBxB4?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI" target="_blank">MIT 6.S191: Convolutional Neural Networks</a>, Alexander Amini, 55min 15s, 2023
___
<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/KuXjwB4LzSA" title="But what is a convolution?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div> 

<a href="https://youtu.be/KuXjwB4LzSA" target="_blank">But what is a convolution?</a>, 3Blue1Brown, 23min 1s, 2022
___
<div class="iframe-wrapper">
    <div class="container">
        <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube-nocookie.com/embed/S8gCPOIFYfM?list=PLlI0-qAzf2Sa6agSVFbrrzyfNkU5--6b_" title="Seq. 02 / Les CNN (Live)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div>
</div>

<a href="https://youtu.be/S8gCPOIFYfM?list=PLlI0-qAzf2Sa6agSVFbrrzyfNkU5--6b_" target="_blank">Seq. 02 / Les CNN (Live)</a>, CNRS - Formation FIDLE, 1h 54min 25s, 2022
___

`````{admonition} 📖 Environnement - Ethique - Impact sociétal
:class: tip
L'intelligence artificielle et en particulier l'**intelligence artificielle générative** a de plus en plus d'impacts sur nos vies. C'est pour cette raison que l'association <a href="https://dataforgood.fr" target="_blank">Data For Good</a> a rédigé un <a href="https://dataforgood.fr/iagenerative/" target="_blank">livre blanc sur les grands défis de l'IA générative</a>.

<div class="container">
  <iframe class="responsive-iframe" src="https://issuu.com/dataforgood/docs/dataforgood_livreblanc_iagenerative_v1.0?fr=sZGE0MjYyNjE5MTU" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 

Suite à ce livre blanc, l'association <a href="https://www.latitudes.cc/" target="_blank">Latitudes</a> a co-construit un **atelier** sur les enjeux de l'intelligence artificielle générative : <a href="https://batailledelia.org" target="_blank">La bataille de l'IA</a>.

<a href="https://www.latitudes.cc/" target="_blank"><img src="https://images.spr.so/cdn-cgi/imagedelivery/j42No7y-dcokJuNgXeA0ig/3d90fb07-6fdf-4c8e-9fdf-47ed9c0e0e47/Untitled/w=640,quality=80" alt="Latitudes Data for good" height="100px"/></a>
`````