<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 🪄 Illusions d'optique

Dans le cours et le TD 1, des <a href="https://fr.wikipedia.org/wiki/Illusion_d%27optique" target="_blank">illusions d'optique</a> basées sur le **constraste** ont été vues. Cette annexe présente différents types d'illusions d'optique sous forme d'un notebook interactif. Vous pouvez donc **modifier** les illusions d'optique pour en **comprendre les limites**.

Pour cette activité complémentaire, vous avez trois possibilités de travail dans l'**écosystème Jupyter** :
- Soit en utilisant les **ressources de votre ordinateur** (Jupyter Lite)

<a href="https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/1a/3pmpols6-td/retro/notebooks/?path=illusion_optique.ipynb" target="_blank"><img src="./_static/phelma_Jupyterlite.png" alt="Jupyter Lite Phelma" height="30px"/></a>

- Soit en utilisant les **ressources d'un serveur distant** (Jupyter Hub - My Binder)

<a href="https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fphelma-sicom%2F1a%2F3pmpols6-td/main?labpath=content%2Fillusion_optique.ipynb" target="_blank"><img src="https://mybinder.org/badge_logo.svg" alt="Jupyter Hub - My Binder" height="25px"/></a>

- Soit en **téléchargeant l'activité** et l'utilisant dans un environnement **Jupyter local** :

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/illusion_optique.ipynb?inline=false" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png" alt="Jupyter Notebook" height="50px"/></a>

- Si vous avez un compte Google, vous pouvez l'ouvrir dans **Colaboratory** :

<a href="https://colab.research.google.com/github/Pierre-Loic/phelma-sicom-3PMPOLS6-td/blob/main/content/illusion_optique.ipynb" target="_blank">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab" height="25px"/>
</a>

___
## Support de l'activité sur les illusions d'optique

<div class="container">
  <iframe class="responsive-iframe" src="https://nbviewer.org/urls/gricad-gitlab.univ-grenoble-alpes.fr/phelma-sicom/1a/3pmpols6-td/-/raw/main/content/illusion_optique.ipynb" width="600" height="340" scrolling="yes" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 