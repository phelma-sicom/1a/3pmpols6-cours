<style>
    /* Pour la gestion des iframes */
 .container {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
</style>

# 📚 Cours : *Détection du contenu d'une image*

Une image est donc un **tenseur de nombres** représentant l'**intensité lumineuse des pixels**. Comment est-il possible de passer de cet ensemble de nombres à la **compréhension du contenu de l'image** ?

## L'apprentissage automatique

### Le fonctionnement général

L'**apprentissage automatique (machine learning)** consiste à trouver une **fonction mathématique** qui fait correspondre une **donnée d'entrée** (dans le cas de la classification d'images, il s'agit des valeurs des intensités lumineuses des pixels de l'image) à une **donnée de sortie** (dans le cas de la classification d'images, il s'agit de la classe de l'image). Le **processus d'entraînement** consiste à **ajuster les paramètres** de cette fonction mathématique afin qu'elle puisse effectuer cette **correspondance** de manière précise grâce à un **jeu de données d'entrainement étiqueté**.

On peut prendre l'exemple simple de **classification binaire** du détection du bâtiment de Minatec :

<img src="./_static/schema_cours_2_1.png" alt="Seuillage" height="150px"/>

La première architecture de réseau de neurones à laquelle on peut penser est un **réseau complétement connecté** (fully connected network) aussi appelé réseau dense. Le réseau de neurones est composé de 3 types de couches de neurones : la **couche d'entrée (input layer)**, **les couches cachées (hidden layers)** et **la couche de sortie (output layer)** {cite}`reseaux_neurones`.

`````{admonition} Mise en pratique

<a href="https://playground.tensorflow.org/" target="_blank"><img src="./_static/playground.png" alt="Tensorflow playground" height="200px"/></a> 

La plateforme <a href="https://playground.tensorflow.org/" target="_blank">Tensorflow Playground</a> permet de comprendre le fonctionnement des réseaux de neurones complétement connectés. Vous pouvez choisir **différentes architectures** de réseaux de neurones et **visualiser l'entrainement** puis le fonctionnement en direct sur des exemples de **classification** et de **régression**.

`````
Les réseaux complétement connectés présentent **certaines limites** pour le traitement des images :

- **Grande dimensionnalité des données** : dans un réseau complètement connecté, chaque **pixel** est traité comme une **entrée distincte**. Ceci nécessite d'optimiser un **grand nombre de paramètres** et peut rendre l'apprentissage du réseau lent. Par exemple, une **image couleur de 100x100** pixels nécessite **30 000 paramètres** (en négligeant les biais de la couche suivante) pour la première couche du réseaux (100x100x3 pour les trois canaux RVB).
- **Perte de structure spatiale** : les réseaux complètement connectés ne tiennent **pas** compte de la **structure spatiale** des images. Ils considèrent **chaque pixel** comme **une entité indépendante** : ils perdent des informations importantes telles que la **proximité spatiale** des pixels et les **motifs locaux**. Pour le traitement d'images, la **structure spatiale** est essentielle pour extraire des **caractéristiques significatives**.
- **Surapprentissage** : les réseaux complètement connectés sont à **risque de surapprentissage sur l'image**. En effet, les réseaux vont apprendre des **détails très fins** des images du jeu d'entrainement et **généraliseront mal**.

### Les étapes du machine learning pour la classification d'images

<img src="./_static/schema_etapes_ML.png" alt="Etapes ML" />

1. **Collecte des images**

La première étape consiste à rassembler un **large ensemble de données** d'images pertinentes pour la **tâche de classification**. Cette collecte peut provenir de bases de données publiques, de caméras ou de collectes spécifiques à un projet. Cette étape s'appuie souvent sur des technologies de **web scraping**. Le nombre d'images nécessaires à l'entrainement du modèle peut être grandement réduit en utilisant du **transfert learning**.

```{note}
Pour obtenir des données pour l'entrainement des modèles, il existe de nombreuses **bases de données ouvertes** (open data). On peut citer par exemple :
- 🛰️ La plateforme <a href="https://www.copernicus.eu/en/access-data" target="_blank">Copernicus</a> (images satellite)
- 🇫🇷 La <a href="https://www.data.gouv.fr/fr/" target="_blank">plateforme ouverte des données publiques françaises</a> (principalement des données tabulaires)
- ⛰️ La <a href="https://data.metropolegrenoble.fr" target="_blank">plateforme des données ouvertes de la métropole de Grenoble</a> (principalement des données tabulaires)
- 🤗 La <a href="https://huggingface.co/datasets" target="_blank">plateforme Hugging Face</a> (tout type de données)
- 🏆 La <a href="https://www.kaggle.com" target="_blank">plateforme de compétitions de data science Kaggle</a> (tout type de données)

📝 Il faut toujours avoir une **traçabilité des jeux de données utilisés** pour entrainer le modèle.
```

2. **Etiquetage des images**

Comme nous traitons du cas de la **classification d'images**, chaque image doit être **étiquetée** avec précision pour identifier la **catégorie** à laquelle elle appartient. L'étiquetage peut être réalisé **manuellement par des humains** ou par des outils semi-automatiques. Cette étape est souvent **longue** et **fastidieuse**.

`````{card} 🏷️ **Ethique de l'étiquetage**
Étant donné que la **qualité** et l'**impartialité** des modèles d'IA dépendent fortement des données avec lesquelles ils sont entraînés, la manière dont ces données sont étiquetées peut avoir de grandes implications éthiques. En particulier dans les domaines suivants :

- **L'exploitation des travailleurs**

Des plateformes comme <a href="https://fr.wikipedia.org/wiki/Amazon_Mechanical_Turk" target="_blank">Amazon Mechanical Turk</a> permettent à des travailleurs de faire de l'étiquetage de données souvent pour une rémunération très faible.

- **La transparence**

Les entreprises doivent être **transparentes** sur la **provenance** des données et la manière dont elles sont étiquetées. Cela inclut la divulgation des méthodes utilisées pour collecter et traiter les données.

- **Les biais dans les données**

Le processus d'étiquetage peut introduire des **biais** si les étiqueteurs ont des **préjugés** conscients ou inconscients. De plus, la manière d'étiqueter dépend de la **culture** et l'**expérience de l'étiqueteur**.
`````

3. **Augmentation des images**

Pour améliorer la **robustesse du modèle**, les images sont souvent modifiées par des **techniques d'augmentation** telles que le **retournement**, le **zoom** ou le **changement de luminosité** afin de générer des données d'entraînement **supplémentaires** à partir de l'ensemble existant.

`````{card} 🐍 **Bibliothèques Python**
Il existe des <a href="./Ressource_1.html#bibliotheques-specialisees-dans-l-augmentation-d-images">bibliothèques Python spécialisées</a> dans l'augmentation d'images. Certaines bibliothèques plus généralistes comme <a href="https://pillow.readthedocs.io/en/stable/" target="_blank">Pillow</a>, <a href="https://docs.opencv.org/4.8.0/" target="_blank">OpenCV</a> et <a href="https://www.tensorflow.org/tutorials/images/data_augmentation?hl=fr" target="_blank">Keras</a> permettent aussi de faire de l'**augmentation de données** pour le traitement d'images.
`````

4. **Choix du modèle**

Le **choix du modèle** est une **étape cruciale** car elle détermine le type de modèle ou d'architecture de réseau de neurones qui sera utilisé pour apprendre à partir des images. Cette décision doit être prise en tenant compte de la **nature de la tâche** (classification, régression, clustering...), de la **complexité des données** et des **contraintes de performance** (temps de traitement, ressources disponibles). Il existe une **grande variété de modèles**. 

`````{card} 🐍 **Bibliothèques Python**
Il existe **plusieurs bibliothèques** pour entrainer des modèles d'apprentissage automatique en Python. La **plus utilisée** pour l'apprentissage automatique hors réseaux de neurones est <a href="https://scikit-learn.org/stable/" target="_blank">scikit-learn</a>. Pour les **réseaux de neurones**, il existe trois grandes bibliothèques : <a href="https://keras.io" target="_blank">keras</a>, <a href="https://www.tensorflow.org/?hl=fr" target="_blank">tensorflow</a> et <a href="https://pytorch.org" target="_blank">pytorch</a>.
`````


5. **Entrainement du modèle**

L'étape d'entraînement implique l'**ajustement des paramètres** du modèle de machine learning sur l'ensemble des données d'entraînement pour qu'il puisse correctement identifier les **catégories des images**.

6. **Evaluation du modèle**

Après l'entraînement, le modèle est évalué en utilisant un ensemble de **données de test** distinct pour vérifier sa précision et sa **capacité à généraliser** sur de nouvelles images.

7. **Déploiement du modèle**

Une fois le modèle évalué et validé, il peut être déployé dans un **environnement de production** où il peut classer de nouvelles images en **temps réel** ou **par lots**. Ce déploiement peut se faire sous la forme d'un **serveur web** ou d'une **API**.

`````{admonition} Solutions de déploiement
Il existe de **très nombreuses solutions** pour déployer des modèles de machine learning. Voici quelques exemples classés du **plus simples** à déployer au **plus compliqué** :
- <a href="https://huggingface.co/" target="_blank">Hugging face</a>
- <a href="https://streamlit.io/" target="_blank">Streamlit</a>
- <a href="https://fastapi.tiangolo.com/" target="_blank">FastAPI</a>
- <a href="https://flask.palletsprojects.com/en/3.0.x/" target="_blank">Flask</a>
- <a href="https://www.djangoproject.com/" target="_blank">Django</a>

Il existe aussi des **modules spécifiques** pour le **déploiement de modèles** dans des **environnements de production** pour les bibliothèques <a href="https://www.tensorflow.org/tfx/tutorials?hl=fr" target="_blank">TensorFlow</a>  et <a href="https://pytorch.org/serve/" target="_blank">PyTorch</a>.
`````

8. **Suivi des performances du modèle**

Il est essentiel de **surveiller en continu les performances** du modèle déployé pour s'assurer qu'il maintient un niveau de précision élevé et pour détecter toute **dérive du modèle**.

`````{admonition} Outils de suivi de performances
Il existe de différents outils de suivi de performances :
- <a href="https://www.tensorflow.org/tensorboard?hl=fr" target="_blank">TensorBoard</a>
- <a href="https://mlflow.org/" target="_blank">MLflow</a>
- <a href="https://wandb.ai/site" target="_blank">WandB (Weights & Biases)</a>
`````

9. **Ré-entrainement du modèle**

Avec l'accumulation de **nouvelles données** ou la **détection de changements** dans les types d'images, le modèle peut **nécessiter un ré-entrainement** pour améliorer ses performances ou s'adapter à de nouvelles catégories.

## L'apprentissage profond avec noyaux de convolution

Avant l'arrivée de l'apprentissage profond (deep learning), les caractéristiques recherchées sur les images étaient créées "à la main". Dans le cadre de l'apprentissage profond, c'est l'algorithme qui **apprend les caractéristiques** grâce aux **noyaux de convolution**. Dans les parties suivantes, nous allons voir les **différents types de couches de neurones** utiles pour concevoir un réseaux de neurones de convolution (CNN).

```{card} 📜 **Histoire de l'intelligence artificielle**
Depuis 2010, les progrès en intelligence artificielle ont transformé la reconnaissance d'image malgré une histoire où les **approches connexionnistes** ont été longtemps **marginalisées** au profit de l'**intelligence artificielle symbolique** {cite}`cardon:hal-02005537`.
```

### Couche de neurones d'entrée

```{important}
Elle ne contient **pas de poids**, elle est définie par la **structure des données d'entrée** (taille de l'image dans notre cas).
```
La couche d'entrée est le **point de départ** de l'architecture du réseau. Elle est responsable de recevoir les **images brutes** en tant qu'entrée. Les caractéristiques de cette couche dépendent de la **taille de l'image** (hauteur et largeur) ainsi que du **nombre de canaux** (3 pour une image en couleurs et 1 pour une image en niveaux de gris). Il est très important que toutes les images d'entrée soient **normalisées** afin qu'elles aient une échelle de valeurs uniforme. Ceci est souvent réalisé par une simple **mise à l'échelle** des valeurs de pixels.

### Couches de neurones de convolution

Les **couches de neurones de convolution** constituent le coeur des réseaux de neurones de convolution (CNN). Elles utilisent des **noyaux de convolution** pour balayer l'image d'entrée (comme cela a été vu dans la première partie de ce cours) et produire des **cartes de caractéristiques**. Ces noyaux sont appris durant le **processus d'entraînement** et sont capables de **détecter** des bords, des coins, des textures et d'autres motifs visuels qui sont très importants pour comprendre le **contenu d'une image**.

Le schéma suivant présente comment **se propage l'information** de l'image en niveaux de gris dans l'**architecture** d'un réseau de neurones avec **deux couches de convolution** (avec 3 et 5 noyaux de convolution) et **deux couches denses** (avec 3 neurones pour chaque couche) :

<a href="./_static/convolution.gif" target="_blank"><img src="./_static/convolution.gif" alt="Couches de convolution" height="400px"/></a>

*Animation créée à l'aide de la bibliothèque <a href="https://github.com/helblazer811/ManimML" target="_blank">ManimML</a> {cite}`manim`*

Chaque noyau de convolution est appliqué à l'image d'entrée avec une certaine stratégie de **déplacement (stride)** et de **remplissage (padding)** pour maintenir les dimensions spatiales. Le résultat de l'opération de convolution passent à travers une **fonction d'activation non-linéaire**. En général, il s'agit de la fonction ReLU (Rectified Linear Unit). En **superposant plusieurs couches de convolution**, un CNN peut apprendre une **hiérarchie de caractéristiques**, des plus simples aux plus complexes, pour une représentation **de plus en plus fine** des images d'entrée.

```{important}
Pour une **image en couleurs** (avec trois canaux RGB), le **noyau de convolution** ne fonctionne pas exactement de la même façon que ce qui a été vu dans la première partie du cours avec **les filtres de convolution**. En effet, dans le cas d'un filtre gaussien, le **filtre de convolution** est appliqué sur les trois canaux **séparément** alors que le noyau de convolution des réseaux de neurones s'applique sur les **trois canaux en même temps** avec des paramètres différents pour les canaux.

Ce point est expliqué dans cette vidéo :

<div class="container">
  <iframe class="responsive-iframe" width="300" height="150" src="https://www.youtube.com/embed/XdTn5md3qTM" title="Source of confusion! Neural Nets vs Image Processing Convolution" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div> 


<a href="https://youtu.be/XdTn5md3qTM" target="_blank">Source of confusion! Neural Nets vs Image Processing Convolution</a>, Animated AI, 9min, 2023
```

### Couches de neurones de pooling

Les couches de pooling **réduisent la dimensionnalité** des cartes de caractéristiques ce qui diminue le nombre de paramètres à traiter. Elle aide également à rendre le modèle **plus invariant** aux petites translations et distorsions dans l'image améliorant ainsi sa **capacité de généralisation**.

```{important}
La couche de **pooling** ne contient **pas de poids**. Aucune information n'est apprise dans cette couche de neurones durant l'entrainement. 
```
Il existe **différentes manières** d'appliquer le pooling : en prenant la **valeur maximale** (MaxPooling) ou la **valeur moyenne** (AveragePooling) par exemple.
Comme pour les couches de neurones de convolution, les hyperparamètres de **stride** et de **padding** peuvent être définis pour les couches de pooling.

`````{card} 🐍 **Bibliothèque Keras - Couche de pooling**
La bibliothèque Keras permet d'appliquer <a href="https://keras.io/api/layers/pooling_layers/" target="_blank">une couche de pooling</a> suivant une (**1D**) ou plusieurs dimensions (**2D ou 3D**). Le code suivant illustre l'**effet d'une couche de MaxPooling2D** sur une matrice **4x4** de **valeurs aléatoires** :
```python
import numpy as np
from keras.layers import MaxPooling2D

data = np.random.randint(1, 10, 16)
print(f"Matrice de données d'entrée :\n{data.reshape(4, 4)}")
data = data.reshape(1,4,4,1)
result = MaxPooling2D(pool_size=(2))(data).numpy()
print(f"Matrice de données après la couche de pooling :\n{result.reshape(2, 2)}")
```
```
Matrice de données d'entrée :
[[4 9 8 9]
 [5 8 8 3]
 [4 5 3 7]
 [5 1 4 8]]
Matrice de données après la couche de pooling :
[[9 9]
 [5 8]]
```
Le code suivant illustre l'**effet d'une couche de AveragePooling2D** sur une matrice **4x4** de **valeurs aléatoires** :
```python
import numpy as np
from keras.layers import AveragePooling2D

data = np.random.randint(1, 10, 16).astype(float)
print(f"Matrice de données d'entrée :\n{data.reshape(4, 4)}")
data = data.reshape(1,4,4,1)
result = AveragePooling2D(pool_size=(2))(data).numpy()
print(f"Matrice de données après la couche de pooling :\n{result.reshape(2, 2)}")
```
```
Matrice de données d'entrée :
[[2. 4. 8. 4.]
 [1. 6. 9. 4.]
 [5. 4. 9. 6.]
 [3. 2. 2. 1.]]
Matrice de données après la couche de pooling :
[[3.25 6.25]
 [3.5  4.5 ]]
```
`````


### Couches de neurones de dropout

Les couches de neurones de **dropout** sont utilisées pour **éviter le surapprentissage (overfitting)**. À chaque passage avant (forward propagation) pour propager les données vers la couche de neurones suivante, un **pourcentage aléatoire** des neurones de la couche de précédente à la couche de dropout est **désactivé**. Ce pourcentage est appelé le **"taux de dropout" (dropout rate)**, et il est généralement un nombre compris entre **0,2** et **0,5**. Pendant l'**inférence**, aucun neurone n'est désactivé : tous les neurones sont **actifs pendant l'inférence**.

```{important}
La couche de **dropout** ne contient **pas de poids**. Aucune information n'est apprise dans cette couche de neurones durant l'entrainement. 
```

L'**objectif principal** du dropout est d'empêcher les neurones de devenir **trop dépendants** les uns des autres pendant l'apprentissage. Cela favorise la **généralisation du modèle** en l'aidant à éviter de s'adapter trop étroitement aux données d'entraînement, ce qui pourrait entraîner un **surapprentissage**.

`````{card} 🐍 **Bibliothèque Keras - Couche de dropout**
Pour ajouter une <a href="https://keras.io/api/layers/regularization_layers/dropout/" target="_blank">couche de neurones de dropout</a> avec la bibliothèque Keras, il suffit d'indiquer à la fonction la **proportion de poids** de neurones à passer à **0** durant la **phase d'entrainement**. Le code suivant illustre l'**effet d'une couche de dropout** sur une matrice **5x2** de **valeurs aléatoires** :
```python
import tensorflow as tf
import numpy as np

layer = tf.keras.layers.Dropout(.5)
data = np.random.rand(5, 2)
outputs = layer(data, training=True)
print(f"""Données d'entrée de la couche de dropout :\n{data}\n
Données de sortie de la couche de dropout :\n{outputs})""")
```
```
Données d'entrée de la couche de dropout :
[[0.07239851 0.87681956]
 [0.26287271 0.1084517 ]
 [0.78026612 0.84411815]
 [0.90008952 0.8139486 ]
 [0.79170717 0.18396941]]

Données de sortie de la couche de dropout :
[[0.         1.7536391 ]
 [0.5257454  0.        ]
 [1.5605322  0.        ]
 [1.800179   1.6278971 ]
 [0.         0.36793882]])
 ```
`````
Le schéma suivant présente comment **se propage l'information** d'une donnée d'entrainement dans l'**architecture** d'un réseau de neurones avec **cinq couches denses** (avec 3, 8, 10, 10 et 4 neurones) et des couches de **dropout** de **0.5** en sortie des couches denses :

<a href="./_static/dropout.gif" target="_blank"><img src="./_static/dropout.gif" alt="Couches de convolution" height="400px"/></a>

*Animation créée à l'aide de la bibliothèque <a href="https://github.com/helblazer811/ManimML" target="_blank">ManimML</a> {cite}`manim`*

### Couche de neurones de sortie

La **couche de sortie d'un CNN** est généralement une **couche entièrement connectée** qui agit comme un **classifieur**. Suite aux transformations des couches de convolution et de pooling, la **couche de sortie** combine ces caractéristiques pour prendre une **décision de classification**. La **fonction d'activation** utilisée dans cette couche **dépend de la tâche spécifique** (ce point est détaillé dans la partie sur les fonctions d'activation). La couche de sortie est l'endroit où le réseau prend sa **décision finale**. C'est là que l'erreur de prédiction est calculée grâce à la **fonction de perte** (ce point est détaillé dans la partie sur les fonctions de perte). Elle est ensuite utilisée pour **mettre à jour les poids** du réseau via la **rétropropagation**.

### Fonctions d'activation

Les fonctions d'activation jouent un rôle important pour les réseaux de neurones car elles permettent d'apporter une **non-linéarité** dans les modèles. **Sans elles**, les réseaux de neurones seraient **uniquement des modèles linéaires**. Les fonctions d'activation sont appliquées après le calcul de la combinaison linéaires des poids et des biais. C'est donc un **hyperparamètre** à définir pour couches de neurones possédant des poids et de biais (couches de convolution et couche de sortie pour les réseaux de convolution).

`````{card} 🐍 **Bibliothèque Keras - Fonctions d'activation**
Les différentes fonctions d'activation utilisables dans la bibliothèque Keras sont détaillées dans la <a href="https://keras.io/api/layers/activations/" target="_blank">documentation</a>. Elles sont utilisables de deux façons :
- soit comme **Activation layer** :
```python
from tensorflow.keras import layers
from tensorflow.keras import activations

model.add(layers.Dense(64))
model.add(layers.Activation(activations.relu))
```

- soit comme **activation argument** d'une couche de neurones :
```python
model.add(layers.Dense(64, activation=activations.relu))
```
ou
```python
model.add(layers.Dense(64, activation='relu'))
```
`````

Voici quelques exemples de fonctions d'activation classiques pour des couches de convolution :
- **ReLU** : qui fournit une sortie positive directe des entrées positives, facilitant des calculs rapides et efficaces.

$\text{ReLU}(x) = \max(0, x)$

ou de manière équivalente :

$\text{ReLU}(x) = 
\begin{cases} 
x & \text{si } x > 0 \\
0 & \text{si } x \leq 0 
\end{cases}$

<img src="./_static/relu.png" alt="Fonction relu"/>

- **Sigmoïde** : qui donne une sortie entre 0 et 1, utile pour modéliser des probabilités.

$\sigma(x) = \frac{1}{1 + e^{-x}}$

<img src="./_static/sigmoide.png" alt="Fonction sigmoide"/>

- **Tanh** : la tangente hyperbolique est similaire à la sigmoïde, mais donne une sortie entre -1 et 1, ce qui la rend centrée sur zéro.

$\tanh(x) = \frac{e^{x} - e^{-x}}{e^{x} + e^{-x}}$

<img src="./_static/tanh.png" alt="Fonction tangente hyperbolique"/>

Pour la **couche de sortie**, la fonction d'activation dépend du type de prédiction recherché :
- Pour de la **classification binaire**, la **fonction sigmoïde** est souvent utilisée car elle peut modéliser la **probabilité** que l'entrée appartienne à la classe positive.
- Pour de la **classification multiclasse**, la fonction **softmax** (ou fonction exponentielle normalisée) est utilisée car elle **généralise la sigmoïde à plusieurs classes** et donne une **probabilité** pour chaque classe, tout en garantissant que la somme de toutes ces probabilités est égale à 1.
___
$\text{softmax}(\mathbf{z})_i = \frac{e^{z_i}}{\sum_{j=1}^{K} e^{z_j}}$

avec :
    
- $\mathbf{z}$ le vecteur des scores (logits) pour chaque classe avant l'activation softmax, avec les composantes $z_i$ pour la i-ème classe.
- $e^{z_i}$ la fonction exponentielle appliquée au score de la i-ème classe.
- $K$ est le nombre total de classes.
- $\sum_{j=1}^{K} e^{z_j}$ la somme des exponentielles de tous les scores, qui sert de normalisateur pour que les sorties de la fonction softmax somment à 1, interprétées comme des probabilités.

<div class="iframe-wrapper">
    <div class="container">
      <video class="responsive-iframe" controls autoplay  loop muted height=300>
        <source src="./_static/Softmax.mp4" type="video/mp4">
      </video>
    </div>
</div>

*Animation créée à l'aide de la bibliothèque <a href="https://www.manim.community" target="_blank">Manim</a> {cite}`manim`*

___

- Pour de la **régression**, où la prédiction est une valeur continue, la fonction d'activation peut être une fonction linéaire (ou pas de fonction du tout), permettant au modèle de prédire une gamme de valeurs continues.

### Fonctions de perte

La **fonction de perte** permet au réseau de neurones d'**apprendre des données**. Elle quantifie l'**écart** entre les **prédictions** et les **données réelles**.

`````{card} 🐍 **Bibliothèque Keras - Fonctions de perte**
Les différentes fonctions de perte (loss functions) utilisables dans la bibliothèque Keras sont détaillées dans la <a href="https://keras.io/api/losses/" target="_blank">documentation</a>. Elles sont définies dans la méthode `compile()` du modèle Keras après avoir défini l'architecture du réseau de neurones :
```python
model.compile(
    loss=tf.keras.losses.BinaryCrossentropy(),
)
```
`````

La sélection de la fonction de perte dépend fortement du type de tâche à réaliser :
- Pour les tâches de **classification binaire**, on peut utiliser l'**entropie croisée** (Cross-Entropy) :

$H(y, \hat{y}) = -\frac{1}{N} \displaystyle\sum_{i=1}^{N} [y_i \log(\hat{y}_i) + (1 - y_i) \log(1 - \hat{y}_i)]$

avec :
- $H(y, \hat{y})$ l'entropie croisée
- $N$ le nombre d'exemples dans le jeu de données
- $y_i$​ la véritable étiquette de classe pour l'exemple i (avec une valeur de 1 ou 0)
- $\hat{y}_i$ la probabilité prédite que l'exemple i appartienne à la classe 1

L'entropie croisée mesure l'écart entre la distribution des étiquettes réelles $y$ et les prédictions $\hat{y}$.

<div class="iframe-wrapper">
    <div class="container">
      <video class="responsive-iframe" controls autoplay  loop muted height=300>
        <source src="./_static/Crossentropy.mp4" type="video/mp4">
      </video>
    </div>
</div>

*Animation créée à l'aide de la bibliothèque <a href="https://www.manim.community" target="_blank">Manim</a> {cite}`manim`*
___
- Pour les tâches de **classification multiclasse**, on peut aussi utiliser l'**entropie croisée** (Cross-Entropy) :

$H(y, \hat{y}) = -\displaystyle\sum_{i=1}^{N} \sum_{c=1}^{M} y_{ic} \log(\hat{y}_{ic})$

avec :
- $H(y, \hat{y})$ l'entropie croisée
- $N$ le nombre d'exemples dans le jeu de données
- $M$ le nombre de classes
- $y_{ic}$​ un indicateur binaire qui vaut 1 si la classe $c$ est la véritable classe pour l'exemple $i$ et 0 autrement
- $\hat{y}_{ic}$​ la probabilité prédite que l'exemple *i* appartienne à la classe *c*
___
- Pour les tâches de **régression**, on peut utiliser l'**erreur quadratique moyenne** (MSE - Mean Squared Error) :

$\text{MSE}(y, \hat{y}) = \frac{1}{N} \displaystyle\sum_{i=1}^{N} (y_i - \hat{y}_i)^2$

Avec :

- $\text{MSE}(y, \hat{y})$ l'erreur quadratique moyenne
- $N$ le nombre d'exemples dans le jeu de données
- $y_i​$ est la valeur réelle
- $\hat{y}_i$​ est la prédiction du modèle pour le i-ème échantillon

### Métriques

Les métriques permettent d'évaluer la **performance des modèles**. Suivant le type de modèle (classification ou régression) et l'importance des différents types d'erreurs, différentes métriques sont utilisées.

**Pour la classification** (ce qui nous concerne pour ce module) :

- **Matrice de confusion** (en anglais *Confusion Matrix*) : tableau qui permet de visualiser les performances d'un algorithme de classification

- **Justesse** ou **Exactitude** (en anglais *Accuracy*) : mesure la **proportion des prédictions correctes** par rapport au **total des prédictions effectuées**. Elle est calculée en divisant le nombre de prédictions correctes (vrais positifs et vrais négatifs) par le nombre total de cas examinés. Pour de la **classification binaire** :

$\text{Accuracy} = \frac{\text{Nombre de prédictions correctes}}{\text{Nombre total de prédictions}}$

$\text{Accuracy} = \frac{TP + TN}{TP + TN + FP + FN}$

Avec :

- $TP$ nombre de vrais positifs (True Positives)
- $TN$ nombre de vrais négatifs (True Negatives)
- $FP$ nombre de faux positifs (False Positives)
- $FN$ nombre de faux négatifs (False Negatives)

<a href="./_static/accuracy_binaire.gif" target="_blank"><img src="./_static/accuracy_binaire.gif" alt="Accuracy binaire" /></a>



## Les architectures de réseaux de convolution

### L'architecture classique

L'**architecture classique** des réseaux de neurones de convolution (CNN) se compose de **couches alternées** de **convolution** et de **pooling** suivies de couches entièrement connectées. Les **couches de convolution** servent à **extraire des caractéristiques** des images tandis que les **couches de pooling** réduisent la **dimensionnalité de ces caractéristiques** pour réduire le temps d'entrainement et le surapprentissage.

Une des **premières architectures** de réseau de neurones de convolution (CNN) est le modèle **LeNet-5**. Il a été conçu pour **reconnaître des caractères manuscrits**. Conçu par **Yann LeCun** et son équipe en **1998**, LeNet-5 est le résultat de plusieurs versions de réseaux destinés à la reconnaissance d'images. Il est considéré comme un des **modèles pionniers** qui ont prouvé l'efficacité des réseaux de neurones de convolution dans le traitement d'image.

L'architecture de LeNet-5 est **relativement simple** comparée aux standards actuels. Elle est résumée dans le tableau suivant :

| Type de Couche    | Taille de la Couche | Taille du Filtre | Stride | Activation |
|-------------------|---------------------|------------------|--------|------------|
| Entrée             | 32x32x1             | N/A              | N/A    | N/A        |
| Convolution | 28x28x6             | 5x5              | 1      | Tanh       |
| Pooling  | 14x14x6             | 2x2              | 2      | Average Pooling |
| Convolution | 10x10x16            | 5x5              | 1      | Tanh       |
| Pooling  | 5x5x16              | 2x2              | 2      | Average Pooling |
| Convolution | 1x1x120             | 5x5              | 1      | Tanh       |
| Complétement connectée | 84                | N/A              | N/A    | Tanh       |
| Sortie (RBF or Softmax) | 10            | N/A              | N/A    | RBF/Softmax|


### L'apprentissage par transfert

L'**apprentissage par transfert** (en anglais transfert learning) est une technique où un modèle développé pour une tâche est réutilisé comme **point de départ** pour un modèle sur une **deuxième tâche**. Dans le contexte des CNN, cela implique souvent d'utiliser des **architectures pré-entraînées** (par exemple VGG, Inception ou ResNet) et de les **adapter** avec un nouvel ensemble de données spécifique. Ceci s'effectue en **modifiant** et en **entraînant les dernières couches** du réseau pour la nouvelle tâche.

<a href="./_static/transfert_learning.png" target="_blank"><img src="./_static/transfert_learning.png" alt="Transfert learning" /></a>

## Les autres types d'utilisation de réseaux de neurones pour l'image

### La détection d'objets

La <a href="https://en.wikipedia.org/wiki/Object_detection" target="_blank">détection d'objets</a> fait appel à des réseaux de neurones pour **localiser** et **identifier** des **objets multiples** dans une image. Cette tâche ne se limite pas à classer une image entière mais vise à fournir une **localisation précise** souvent sous la forme de boîtes englobantes (bounding box) pour chaque objet détecté.

<a href="https://en.wikipedia.org/wiki/Object_detection" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Detected-with-YOLO--Schreibtisch-mit-Objekten.jpg/1920px-Detected-with-YOLO--Schreibtisch-mit-Objekten.jpg" alt="Détection d'objets"/></a>

*Source de l'image : wikipedia*

### La segmentation sémantique et d'instance

La <a href="https://en.wikipedia.org/wiki/Image_segmentation" target="_blank">segmentation sémantique</a> utilise des réseaux de neurones pour **classer chaque pixel** d'une image dans une catégorie ce qui permet une compréhension détaillée de la scène.

La **segmentation d'instance** va plus loin en distinguant non seulement les catégories d'objets mais aussi les **différentes instances** d'une même catégorie.

<a href="https://en.wikipedia.org/wiki/Image_segmentation" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/b/bd/3D_CT_of_thorax.jpg" alt="Segmentation d'image 3D"/></a>


*Source de l'image : segmentation 3D des vaisseaux d'un poumon, wikipedia*

### La génération d'images

Les réseaux de neurones sont également capables de **générer de nouvelles images** qui n'existent pas dans le monde réel. Ils se basent sur **différentes technologies**. Voici les principales :

- **Auto-encodeurs variationnels (VAE)**

Les <a href="https://fr.wikipedia.org/wiki/Auto-encodeur_variationnel" target="_blank">auto-encodeurs variationnels</a> (en anglais variational auto encoder - VAE) sont un type de réseau de neurones qui apprend à encoder des données en une **distribution latente** pour ensuite les décoder et **reconstruire les données d'entrée**. Dans le contexte de la génération d'images, les VAE sont entraînés pour produire des images qui ressemblent à celles de l'ensemble de données d'origine en générant des points dans l'espace latent qui sont ensuite décodés en nouvelles images.

<a href="./_static/VAE.png" target="_blank"><img src="./_static/VAE.png" alt="Transfert learning" /></a>

- **Réseaux antagonistes génératifs**

Les <a href="https://fr.wikipedia.org/wiki/R%C3%A9seaux_antagonistes_g%C3%A9n%C3%A9ratifs" target="_blank">réseaux antagonistes génératifs</a> (en anglais generative adversarial networks - GAN) sont composés de **deux réseaux de neurones distincts** (le générateur et le discriminateur) qui sont entraînés simultanément. Le **générateur** essaie de créer des images qui sont **indiscernables des images réelles** tandis que le **discriminateur** tente de **distinguer les fausses images des vraies**. Ce processus concurrentiel pousse le générateur à produire des images de plus en plus réalistes.

<a href="./_static/GAN.png" target="_blank"><img src="./_static/GAN.png" alt="Réseaux antagonistes génératifs" /></a>

- **Modèles de diffusion**

Les <a href="https://en.wikipedia.org/wiki/Diffusion_model" target="_blank">modèles de diffusion</a> sont une classe **relativement nouvelle** de générateurs d'images qui fonctionnent en **inversant progressivement** un processus de diffusion qui détruit les données d'une image. À chaque étape, le modèle prédit une version **légèrement moins bruitée** de l'image jusqu'à ce qu'une image claire soit générée. Ces modèles sont connus pour leur capacité à **générer des images de haute qualité** et sont considérés comme une alternative aux GAN.

<a href="https://en.wikipedia.org/wiki/Diffusion_model" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/X-Y_plot_of_algorithmically-generated_AI_art_of_European-style_castle_in_Japan_demonstrating_DDIM_diffusion_steps.png/1920px-X-Y_plot_of_algorithmically-generated_AI_art_of_European-style_castle_in_Japan_demonstrating_DDIM_diffusion_steps.png" alt="Modèle de diffusion"/></a>

*Source de l'image : wikipedia*

```{card} 🎨 **Impact sur la créativité**
L'intelligence artificielle **générative** est une nouvelle technologie qui **bouleverse** le monde de l'image. La vidéo suivante analyse l'image <a href="https://en.wikipedia.org/wiki/Théâtre_D%27opéra_Spatial" target="_blank">Théâtre d'opéra spatial</a> générée à l'aide de l'outil <a href="https://en.wikipedia.org/wiki/Midjourney" target="_blank">Midjourney</a> et qui a gagné un prix à la foire d'art du Colorado en 2022 :

<div class="iframe-wrapper">
    <div class="container">
<iframe class="responsive-iframe" title="Le dessous des images" allowfullscreen="true" frameborder="0" scrolling="no" src="https://www.arte.tv/embeds/fr/110342-003-A?autoplay=0"></iframe>
    </div>
</div>
```

Les **avancées rapides** en intelligence artificielle **générative** entraînent une **prolifération de données synthétiques**. Ceci aboutit à une situation où il pourrait y avoir **plus de données synthétiques que réelles** sur Internet. Cette utilisation croissante de données synthétiques pour l'**entraînement des modèles génératifs** crée des **boucles autophages** (analogie avec la <a href="https://fr.wikipedia.org/wiki/Crise_de_la_vache_folle" target="_blank">maladie de la vache folle</a>) potentiellement problématiques avec des conséquences peu comprises sur les biais, la qualité et la diversité des modèles génératifs {cite}`selfconsuming`.

___
## Support de présentation du cours

<a href="./_static/slides/cours_IA_Images.pdf" target="_blank">👨‍🏫 Support de cours N°2</a>

<div class="container">
  <iframe class="responsive-iframe" src="./_static/slides/cours_IA_Images.pdf" width="600" height="340" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>
</div> 

