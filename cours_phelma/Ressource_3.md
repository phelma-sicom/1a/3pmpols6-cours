# 🔤 Terminologie

## 🤖 Vocabulaire de l'intelligence artificielle

### Surapprentissage (overfitting)

Le **surapprentissage** en machine learning se produit lorsque le modèle d'apprentissage automatique s'**adapte trop bien** aux **données d'entraînement** : il **perd sa capacité à généraliser** efficacement sur de nouvelles données. Cela se traduit par une **mauvaise performance** sur des **exemples inconnus** car le modèle a mémorisé les données d'entraînement au lieu d'apprendre des motifs utiles.
___

<iframe width="300" height="150" src="https://www.youtube.com/embed/EuBBz3bI-aA" title="Machine Learning Fundamentals: Bias and Variance" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<a href="https://youtu.be/EuBBz3bI-aA" target="_blank">Machine Learning Fundamentals: Bias and Variance</a>, StatQuest, 6min 35s, 2018
___

### Généralisation

La **généralisation** en machine learning correspond à la capacité d'un modèle à **appliquer des connaissances apprises** sur des **données d'entraînement** à de **nouvelles données non vues auparavant**.

### Inférence

L'inférence en machine learning désigne le processus par lequel un **modèle entraîné** est utilisé pour faire des **prédictions** sur de **nouvelles données non vues**.

### Hyperparamètre

Un hyperparamètre est une **caractéristique externe** du modèle qui n'est **pas apprise** à partir des données mais est **définie à l'avance** et reste **constante** pendant l'entrainement. Ces hyperparamètres incluent par exemple le **taux d'apprentissage**, le nombre de couches cachées, le nombre de neurones par couche et la taille des lots d'entraînement (batch size).

## 📷 Vocabulaire de l'image

### Formats d'images

Les images peuvent être stockées sous **différents format**. Voici quelques exemples :
- **Bitmap (BMP)** : les fichiers BMP 24 bits stockent l'**image matricielle non compressée** avec un octet par couleur primaire (RVB) 
- **Joint Photographic Experts Group (JPEG)** : les fichiers JPEG permettent de stocker des **images matricielles compressées** mais avec une **perte de qualité** par rapport à l'image d'origine 
- **Tagged Image File Format (TIFF)** : les fichiers TIFF permettent de stocker des **images matricielles compressées sans perte**
- **Portable Network Graphics (PNG)** : les fichiers PNG permettent de stocker des **images matricielles compressées sans perte** et d'ajouter de la **transparence** à l'image grâce à un canal supplémentaire
- **Graphics Interchange Format (GIF)** : les fichiers GIF permettent de stocker des **images matricielles ou des animations courtes compressées avec perte**
- **Scalable Vector Graphics (SVG)** : les fichiers SVG permettent de stocker des **images vectorielles**