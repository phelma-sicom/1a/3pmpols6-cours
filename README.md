<img src='./cours_phelma/logo.png' height='100'/> 

# Plateforme de cours du module de preorientation SICOM - Traitement du Signal et Multimedia - 3PMPOLS6
<img src='https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc.svg' height='31'/>

___

🌐 Lien vers la **plateforme de cours** : https://phelma-sicom.gricad-pages.univ-grenoble-alpes.fr/1a/3pmpols6-cours

___

Ce cours est une initiation à l'intelligence artificielle **orientée image**. Il est construit en **deux parties** :

- *Partie N°1 : introduction à la notion d'image en informatique*

- *Partie N°2 : détection du contenu d'une image*